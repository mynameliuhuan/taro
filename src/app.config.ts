export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/home/home',
    'pages/wallpaper/wallpaper',
    'pages/person/person',
  ],

  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
 
})