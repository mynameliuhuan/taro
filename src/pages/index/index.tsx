import { View, Text, Form, Input, Button } from '@tarojs/components'
import { AtButton, AtToast } from 'taro-ui'
import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.scss'
// eslint-disable-next-line import/first
import { useState } from 'react'

// eslint-disable-next-line import/first
import _ from 'lodash'
// eslint-disable-next-line import/first
import Taro from '@tarojs/taro'

function Index() {
  const [login, setLogin] = useState({ name: 'liuhuan', password: 'sunyan' })
  const submit = (e) => {
    console.log(e)
    console.log(_.isEqual(login, e.target.value))
    if (_.isEqual(login, e.target.value)) {
      Taro.showToast({
        title: '登录成功',
        icon: 'success',
        duration: 2000
      })
      Taro.redirectTo({
        url: '/pages/home/home'
      });
    } else {
      Taro.showToast({
        title: '登录失败',
        icon: 'success',
        duration: 2000
      })
    }
  }
  return (
    <View className='login-container'>
      <View className='login-box'>
        <View className='font-head'>登录</View>
        <View className='login-form'>
          <Form onSubmit={submit}>
            <View className='btn-foot'>
              <View>
                <Input placeholder='请输入用户名' type='text' name='name' />
              </View>
              <View className='btn-item'>
                <Input placeholder='请输入密码' password name='password' />
              </View>

              <View className='btn-item'>
                <Button type='primary' formType='submit'>提交</Button>
              </View>

              <View className='btn-item'>
                <Button formType='reset'>重置</Button>
              </View>

            </View>
          </Form>
        </View>
      </View>
    </View>
  )
}

export default Index

