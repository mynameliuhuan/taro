import { View, Image, ScrollView } from '@tarojs/components'
import Taro, { Config } from '@tarojs/taro'
import './index.scss'
import Img from '../../../assets/images/loginbg.jpeg'

function Wallpaper() {
const previewImg=()=>{
    Taro.previewImage({
        current: "https://img2.baidu.com/it/u=1124591381,1037079365&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=753", // 当前显示图片的http链接
        urls: ["https://img2.baidu.com/it/u=1124591381,1037079365&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=753"] // 需要预览的图片http链接列表
      });
}
    return (
        <ScrollView scrollY type='list'  className='scroll-box' lowerThreshold={90} >
            <View className='wallpaper-container'>
                <Image className='wallpaper-item'  src={Img} onClick={previewImg} />
                <Image className='wallpaper-item' src={Img} />
                <Image className='wallpaper-item' src={Img} />
                <Image className='wallpaper-item' src={Img} />
                <Image className='wallpaper-item' src={Img} />
            </View>
        </ScrollView>

    )
}

export default Wallpaper