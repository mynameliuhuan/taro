import Taro from '@tarojs/taro';
import React, { useEffect, useState } from 'react'
import { AtTabBar } from 'taro-ui'
import Wallpaper from './Component/Wallpaper';
import MessageBoard from './Component/MessageBoard';
import Person from './Component/Person';

function Home() {
  const [tabPage, setTabpage] = useState(0)


  const content = (index) => {
    switch (index) {
      case 0:
        return <Wallpaper />
      case 1:
        return <MessageBoard />
      case 2:
        return <Person />
      default:
        return <Wallpaper />
    };
  }


  return (
    <>
      {content(tabPage)}
      <AtTabBar
        fixed
        selectedColor='red'
        tabList={[
          { title: '壁纸', iconType: 'image' },
          { title: '留言板', iconType: 'bullet-list' },
          { title: '我的', iconType: 'user' }
        ]}
        onClick={(index, event) => {
          setTabpage(index)
        }}
        current={tabPage}
      />
    </>
  )
}

export default Home